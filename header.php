<nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01" >
        <a class="navbar-brand my-3" href="accueil.html" class = "text-success" style="font-size: 2em;"> <i class="fa-brands fa-yoast fa-3x text-success"></i>Play </a>
        <ul class="navbar-nav me-auto mb-2 mb-lg-0  " style="margin-left: 1À0px;">
            <li class="nav-item " style="margin-left: 50px; font-size: 1.5em;">
            <a class="nav-link active text-success" aria-current="page" href="acceuil.php"> Accueil </a>
            </li>
            <li class="nav-item " style="margin-left: 120px; font-size: 1.5em;">
            <a class="nav-link active" href="#" tabindex="-1" aria-disabled="true"> A propos </a>
            </li>
        </ul>
        <form class="d-flex ">
            <a class="btn btn-success m-3" href="inscription.php"> Inscription </a>
        </form>
        </div>
    </div>
</nav>
        