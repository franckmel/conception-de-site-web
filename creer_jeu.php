<?php
    include '../controller/connect.php';
    $message = '';
    if(isset($_POST['submit'])){
      $nom = $_POST['nom'];
      $desc = $_POST['desc'];
      $regle = $_POST['regle'];
      $categorie = $_POST['categorie'];
      $photo = $_POST['photo'];
      if($nom != '' && $categorie != ''){
        $insert_jeu = $conn->prepare("INSERT INTO `jeux`(nom, description,regle,photo,categorie) VALUES(?,?,?,?,?)");
        $insert_jeu->execute([$nom, $desc,$regle,$photo,$categorie]);
        $message = 'Jeu enregistré avec succes';
      }else{
        $message = 'Le nom/La categorie, n ont pas été renseigné';
        
      }
    }
?>


<!DOCTYPE html5>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            jeu
        </title>

        <!-- Liens CDN de Bootstrap -->
        <link rel="stylesheet" type="text/css" href="../style/style.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-xCs+HLwMAnqguRZQTO4tMyHiMXJcFASWJu+1XCx0Vp5q+BlBaa+ZuVmFcqZOw83yHxG83u0eLuRU2LNN4mXFDQ==" crossorigin="anonymous" /> -->
    </head>

    <body>

        <!-- barre de navigation -->
        <?php include '../view/header_admin.php'?>

        <div class="container bg-light border border-success">
            <h1> Formulaire de creation de jeu   </h1>
            <div class="mb-3">
                <span><?=$message?></span>
            </div>
            <form method="post">
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Titre</label>
                  <input type="text" class="form-control" name="nom" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Description</label>
                    <input type="text" class="form-control" name="desc" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Regle</label>
                    <input type="text" class="form-control" name="regle" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Categorie </label>
                    <input class="form-control" name="categorie" aria-label="Default select example">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Photo</label>
                    <input type="text" class="form-control" name="photo" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <button type="submit" name="submit" class="btn btn-success">Creer </button>
              </form>
        </div>

        <!-- footer -->
       <?php include '../view/footer.php'?>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>

</html> 