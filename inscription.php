<?php
    include '../controller/connect.php';
    $message = '';
    if(isset($_POST['submit'])){
      $login = $_POST['login'];
      $mail = $_POST['email'];
      $pass = $_POST['pass'];
      $cpass = $_POST['cpass'];
      $statut = $_POST['statut'];
      if($pass != $cpass){
        $message = 'Les mots de passes sont différent!';}
      else
      if($login != '' && $mail != '' && $pass != '' && $statut != ''){
        $insert_user = $conn->prepare("INSERT INTO `user`(login,email,password,status) VALUES(?,?,?,?)");
        $insert_user->execute([$login,$mail,$pass,$statut]);
        $message = 'Utilisateur enregistré avec succes';
      }else{
        $message = 'Des champs n ont pas été renseigné';
        
      }
      
      
    }

?>


<!DOCTYPE html5>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            inscription
        </title>

        <!-- Liens CDN de Bootstrap -->
        <link rel="stylesheet" type="text/css" href="../style/style.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-xCs+HLwMAnqguRZQTO4tMyHiMXJcFASWJu+1XCx0Vp5q+BlBaa+ZuVmFcqZOw83yHxG83u0eLuRU2LNN4mXFDQ==" crossorigin="anonymous" /> -->
    </head>

    <body>

        <!-- barre de navigation -->
        <?php include '../view/header_admin.php'?>

        <div class="container bg-light border border-success">
            <h1> Formulaire d'inscription  </h1>
            <div class="mb-3">
                <span><?=$message?></span>
            </div>
            <form method="post">
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Adresse mail</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Login</label>
                    <input type="text" class="form-control" name="login" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">Mot de passe  </label>
                  <input type="password" class="form-control" name="pass" id="exampleInputPassword1">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label"> Confirmer le Mot de passe  </label>
                    <input type="password" class="form-control" name="cpass" id="exampleInputPassword1">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Statut </label>
                    <select class="form-select" name="statut" aria-label="Default select example">
                        <option selected>choisissez un statut</option>
                        <option value="membre">Membre </option>
                        <option value="admin">Administrateur</option>
                      </select>
                </div>
                <button type="submit" name="submit" class="btn btn-success">Ajouter</button>
              </form>
        </div>

        <!-- footer -->
        <?php include '../view/footer.php'?>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>

</html> 