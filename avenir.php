<?php
    include 'controller/connect.php';
    $planning = $conn->prepare("SELECT * FROM `planning` WHERE etat = 'à venir'"); 
    $planning->execute();
    $message = '';
    if(isset($_POST['submit'])){
        $ins = $conn->prepare("SELECT * FROM `inscription` WHERE id_planning = ? and id_user = ?"); 
        $ins->execute([$_POST['submit'],$user_id]);
        if($ins->rowCount()>0)
        $message = 'Vous etes dejas inscrit'; 
        else{
        $inscrire = $conn->prepare("INSERT INTO `inscription`(id_planning,id_user) VALUES (?,?)");
        $inscrire->execute([$_POST['submit'],$user_id]);
        $message = 'Inscription enregistre';
        }
    }

?>

<!DOCTYPE html5>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            a venir
        </title>

        <!-- Liens CDN de Bootstrap -->
        <link rel="stylesheet" type="text/css" href="style/style.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-xCs+HLwMAnqguRZQTO4tMyHiMXJcFASWJu+1XCx0Vp5q+BlBaa+ZuVmFcqZOw83yHxG83u0eLuRU2LNN4mXFDQ==" crossorigin="anonymous" /> -->
    </head>

    <body>

        <!-- barre de navigation -->
        <?php include 'view/header_user.php'?> 
      
        <!-- tableau qui va permettre de gerer facilement les membres  -->
        <div class="container">
            <h1 class="page-header"> Parties à venir </h1>
            <div class="mb-3">
                <span><?=$message?></span>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered table-sm">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Jeu </th>
                    <th>crenaux </th>
                    <th>Inscrits </th>
                    </tr>
                </thead>
                <tbody>
                <?php if($planning->rowCount() > 0){
                        $i=1;
                     while($plan = $planning->fetch(PDO::FETCH_ASSOC)){
                        $jeux = $conn->prepare("SELECT nom FROM `jeux` WHERE id = ?"); 
                        $jeux->execute([$plan['id_jeu']]);
                        $jeu = $jeux->fetch(PDO::FETCH_ASSOC);
                        $inscrits = $conn->prepare("SELECT id FROM `inscription` WHERE id_planning = ?"); 
                        $inscrits->execute([$plan['id']]);?>
                    <tr>
                        <td><?=$i ?></td>
                        <td><?=$jeu['nom']; ?></td>
                        <td><?=$plan['date']; ?></td>
                        <td><?=$inscrits->rowCount(); ?></td>
                        
                        
                        <td> <form method="post"><button value="<?=$plan['id']; ?>" type="submit" name="submit" class= "btn btn-success" > <i class="fas fa-trash"></i> &ensp; S'inscrire</button> </form></td>
            
                    </tr>
                    <?php $i = $i+1;}}?>
                </tbody>
                </table>
            </div>
        </div>
        <!-- footer -->
        <?php include 'view/footer.php'?>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>

</html> 