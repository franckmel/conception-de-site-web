<?php
    include '../controller/connect.php';
    $message = '';
    if(isset($_POST['submit'])){
      $id_jeu = $_POST['id_jeu'];
      $date = $_POST['date'];
      if($id_jeu != '' && $date != ''){
        $insert_plan = $conn->prepare("INSERT INTO `planning`(id_jeu,date,etat) VALUES(?,?,'à venir')");
        $insert_plan->execute([$id_jeu,$date]);
        $message = 'Crenau enregistré avec succes';
      }else{
        $message = 'Des champs n ont pas été renseignés';
        
      }
      
      
    }

?>


<!DOCTYPE html5>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            Planning
        </title>

        <!-- Liens CDN de Bootstrap -->
        <link rel="stylesheet" type="text/css" href="../style/style.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-xCs+HLwMAnqguRZQTO4tMyHiMXJcFASWJu+1XCx0Vp5q+BlBaa+ZuVmFcqZOw83yHxG83u0eLuRU2LNN4mXFDQ==" crossorigin="anonymous" /> -->
    </head>

    <body>

        <!-- barre de navigation -->
        <?php include '../view/header_admin.php'?>

        <div class="container bg-light border border-success">
            <h1> Nouveau Crenau  </h1>
            <div class="mb-3">
                <span><?=$message?></span>
            </div>
            <form method="post">
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Statut </label>
                    <select class="form-select" name="id_jeu" aria-label="Default select example">
                        <option selected>choisissez un Jeu</option>
                        <?php   $jeux = $conn->prepare("SELECT * FROM `jeux`"); 
                                $jeux->execute();
                            if($jeux->rowCount() > 0){
                             while($jeu = $jeux->fetch(PDO::FETCH_ASSOC)){?>

                        <option value="<?=$jeu['id']?>"><?= $jeu['nom']?> </option>

                        <?php }}?>
                      </select>
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Crenau</label>
                    <input type="text" class="form-control" name="date" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <button type="submit" name="submit" class="btn btn-success">Ajouter</button>
              </form>
        </div>

        <!-- footer -->
        <?php include '../view/footer.php'?>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>

</html> 