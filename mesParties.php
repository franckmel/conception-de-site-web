<?php
    include 'controller/connect.php';
    $parties = $conn->prepare("SELECT * FROM `inscription` i INNER JOIN `planning` p ON p.id = i.id_planning WHERE i.id_user = ?"); 
    $parties->execute([$user_id]);
?>

<!DOCTYPE html5>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            parties
        </title>

        <!-- Liens CDN de Bootstrap -->
        <link rel="stylesheet" type="text/css" href="style/style.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-xCs+HLwMAnqguRZQTO4tMyHiMXJcFASWJu+1XCx0Vp5q+BlBaa+ZuVmFcqZOw83yHxG83u0eLuRU2LNN4mXFDQ==" crossorigin="anonymous" /> -->
    </head>

    <body>

        <!-- barre de navigation -->
        <?php include 'view/header_user.php'?> 
      
        <!-- tableau qui va permettre de gerer facilement les membres  -->
        <div class="container">
            <h1 class="page-header"> Mes Parties</h1>
    
            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered table-sm">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Jeu </th>
                    <th>crenaux </th>
                    <th>etat </th>
                    </tr>
                </thead>
                <tbody>
                <?php if($parties->rowCount() > 0){
                        $i=1;
                     while($plan = $parties->fetch(PDO::FETCH_ASSOC)){
                        $jeux = $conn->prepare("SELECT nom FROM `jeux` WHERE id = ?"); 
                        $jeux->execute([$plan['id_jeu']]);
                        $jeu = $jeux->fetch(PDO::FETCH_ASSOC);
                        ?>
                    <tr>
                        <td><?=$i ?></td>
                        <td><?=$jeu['nom']; ?></td>
                        <td><?=$plan['date']; ?></td>
                        <td><?=$plan['etat'] ?></td>
                    </tr>
                    <?php $i = $i+1;}}?>
                </tbody>
                </table>
            </div>
        </div>
        <!-- footer -->
        <?php include 'view/footer.php'?>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>

</html> 