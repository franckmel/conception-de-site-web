<?php
    include '../controller/connect.php';
    $message = '';
    $update_id = $_GET['update'];

    if(isset($_POST['submit'])){
      $nom = $_POST['nom'];
      $desc = $_POST['desc'];
      $regle = $_POST['regle'];
      $categorie = $_POST['categorie'];
      $photo = $_POST['photo'];
      if($nom !='' ){
        $update_jeu = $conn->prepare("UPDATE `jeux` SET nom = ? WHERE id = ?");
        $update_jeu->execute([$nom, $update_id]);
      }
      if($desc !='' ){
        $update_jeu = $conn->prepare("UPDATE `jeux` SET description = ? WHERE id = ?");
        $update_jeu->execute([$desc, $update_id]);
      }
      if($categorie !='' ){
        $update_jeu = $conn->prepare("UPDATE `jeux` SET categorie = ? WHERE id = ?");
        $update_jeu->execute([$categorie, $update_id]);
      }
      if($regle !='' ){
        $update_jeu = $conn->prepare("UPDATE `jeux` SET regle = ? WHERE id = ?");
        $update_jeu->execute([$regle, $update_id]);
      }
      if($photo !='' ){
        $update_jeu = $conn->prepare("UPDATE `jeux` SET photo = ? WHERE id = ?");
        $update_jeu->execute([$photo, $update_id]);
      }
      
      
      $message = 'Jeu modifié avec succes';
    }

?>


<!DOCTYPE html5>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            jeu
        </title>

        <!-- Liens CDN de Bootstrap -->
        <link rel="stylesheet" type="text/css" href="../style/style.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-xCs+HLwMAnqguRZQTO4tMyHiMXJcFASWJu+1XCx0Vp5q+BlBaa+ZuVmFcqZOw83yHxG83u0eLuRU2LNN4mXFDQ==" crossorigin="anonymous" /> -->
    </head>

    <body>

        <!-- barre de navigation -->
        <?php include '../view/header_admin.php'?>

        <div class="container bg-light border border-success">
            <h1>Modifier le jeu   </h1>
            <div class="mb-3">
                <span><?=$message?></span>
            </div>
            <?php
                $jeu_select = $conn->prepare("SELECT * FROM `jeux` WHERE id = ?");
                $jeu_select->execute([$update_id]);
                if($jeu_select->rowCount() > 0){
                    $jeu = $jeu_select->fetch(PDO::FETCH_ASSOC); 
            ?>
            <form method="post">
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Nom</label>
                  <input type="text" class="form-control" name="nom" value="<?= $jeu['nom'];?>" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Description</label>
                    <input type="text" class="form-control" name="desc" value="<?= $jeu['description'];?>" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Regle</label>
                    <input type="text" class="form-control" name="regle" value="<?= $jeu['regle'];?>" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Categorie </label>
                    <input class="form-control" name="categorie"value="<?= $jeu['categorie'];?>" aria-label="Default select example">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Photo</label>
                    <input type="text" class="form-control" name="photo" value="<?= $jeu['photo'];?>" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <button type="submit" name="submit" class="btn btn-success">Modifier </button>
                
              </form>
              <?php }?>
        </div>

        <!-- footer -->
       <?php include '../view/footer.php'?>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>

</html> 