<?php

include 'controller/connect.php';

if(isset($_POST['submit'])){

   $login = $_POST['login'];
   $pass = $_POST['pass'];

   $select_user = $conn->prepare("SELECT * FROM `user` WHERE login = ? AND password = ?");
   $select_user->execute([$login, $pass]);
   $row = $select_user->fetch(PDO::FETCH_ASSOC);

   if($select_user->rowCount() > 0){
      $_SESSION['user_id'] = $row['id'];
      if($row['status'] == 'admin')
        header('location:admin/admin.php');
      else
      header('location:acceuil.php');
   }else{
      $message[] = 'Email/Mot de passe incorrect!';
   }

}

?>

<!DOCTYPE html5>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            se connecter
        </title>

        <!-- Liens CDN de Bootstrap -->
        <link rel="stylesheet" type="text/css" href="style/style.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-xCs+HLwMAnqguRZQTO4tMyHiMXJcFASWJu+1XCx0Vp5q+BlBaa+ZuVmFcqZOw83yHxG83u0eLuRU2LNN4mXFDQ==" crossorigin="anonymous" /> -->
    </head>

    <body>

        <!-- barre de navigation -->
        <?php include 'view/header.php';?>
        <div class="container bg-light border border-success">
            <h1> Formulaire de connexion  </h1>
            <form method="post">
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Login</label>
                    <input type="text" class="form-control" name="login" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">Mot de passe  </label>
                  <input type="password" class="form-control" name="pass" id="exampleInputPassword1">
                </div>
                <button type="submit" name="submit" class="btn btn-success">Se Connecter </button>
              </form>
        </div>

        <!-- footer -->
        <?php include 'view/footer.php'?>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>

</html> 