-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema association_jeu
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema association_jeu
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `association_jeu` DEFAULT CHARACTER SET utf8 ;
USE `association_jeu` ;

-- -----------------------------------------------------
-- Table `association_jeu`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `association_jeu`.`user` (
  `id` INT NOT NULL,
  `login` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `association_jeu`.`jeux`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `association_jeu`.`jeux` (
  `id` INT NOT NULL,
`nom` varchar(40) NOT NULL,
  `description` VARCHAR(1000) NULL,
  `categorie` varchar(45) NOT NULL,
  `regle` VARCHAR(1000) NULL,
  `photo` varchar(128) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `association_jeu`.`souhait`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `association_jeu`.`souhait` (
  `id` INT NOT NULL,
  `id_jeu` INT NOT NULL,
  `id_user` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_souhait_jeux1`
    FOREIGN KEY (`id_jeu`)
    REFERENCES `association_jeu`.`jeux` (`id`),
  CONSTRAINT `fk_souhait_user1`
    FOREIGN KEY (`id_user`)
    REFERENCES `association_jeu`.`user` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `association_jeu`.`planning`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `association_jeu`.`planning` (
  `id` INT NOT NULL,
  `id_jeu` INT NOT NULL,
  `date` VARCHAR(50) NOT NULL,
  `etat` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_planning_jeux1`
    FOREIGN KEY (`id_jeu`)
    REFERENCES `association_jeu`.`jeux` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `association_jeu`.`inscription`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `association_jeu`.`inscription` (
  `id` INT NOT NULL,
  `id_planning` INT NOT NULL,
  `id_user` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_inscription_planning1`
    FOREIGN KEY (`id_planning`)
    REFERENCES `association_jeu`.`planning` (`id`),
  CONSTRAINT `fk_inscription_user1`
    FOREIGN KEY (`id_user`)
    REFERENCES `association_jeu`.`user` (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
