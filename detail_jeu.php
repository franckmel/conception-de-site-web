<!DOCTYPE html5>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            {% block title %}
            {% endblock %}
        </title>

        <!-- Liens CDN de Bootstrap -->
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-xCs+HLwMAnqguRZQTO4tMyHiMXJcFASWJu+1XCx0Vp5q+BlBaa+ZuVmFcqZOw83yHxG83u0eLuRU2LNN4mXFDQ==" crossorigin="anonymous" /> -->
    </head>

    <body>

        <!-- barre de navigation -->

        <nav class="navbar navbar-expand-lg navbar-light bg-light ">
            <div class="container-fluid">
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarTogglerDemo01 m-5" >
                <a class="navbar-brand my-3" href="accueil.html" class = "text-success" style="font-size: 2em;"> <i class="fa-brands fa-yoast fa-3x text-success"></i>Play </a>
                <ul class="navbar-nav me-auto mb-2 mb-lg-0  " style="margin-left: 1À0px;">
                  <li class="nav-item " style="margin-left: 50px; font-size: 1.5em;">
                    <a class="nav-link active text-success" aria-current="page" href="accueil.html"> Accueil </a>
                  </li>
                  <li class="nav-item " style="margin-left: 120px; font-size: 1.5em;">
                    <a class="nav-link active"  href="#"> Documentation</a>
                  </li>
                  <li class="nav-item " style="margin-left: 120px; font-size: 1.5em;">
                    <a class="nav-link active" href="#" tabindex="-1" aria-disabled="true"> A propos </a>
                  </li>
                </ul>
                <form class="d-flex ">
                  <a class="btn btn-outline-success m-3" href="connexion.html"> Connexion </a>
                  <a class="btn btn-success m-3" href="inscription.html"> Inscription </a>
                  <a class="btn btn-primary m-3" href="admin.html"> Admin </a>
                </form>
              </div>
            </div>
        </nav>


        <!-- presentation du jeu  -->

        <div class="container">
            <a class="btn btn-primary m-3" href="accueil.html"> Retour </a>
            <h1> Présentation de notre jeu </h1>
            <ul>
                <li>
                    <h5>
                        Photo principale:
                    </h5>
                </li>
                <li>
                    <h5>
                        Titre:
                    </h5>
                </li>
                <li>
                    <h5>
                        Description:
                    </h5>
                </li>
                <li>
                    <h5>
                        Categorie:
                    </h5>
                </li>
                <li>
                    <h5>
                        Règle:
                    </h5>
                </li>
                <li>
                    <h5>
                        Photo supplémentaire:
                    </h5>
                </li>
                <li>
                    <h5>
                        Echéance de jeu prévu:
                    </h5>
                </li>
            </ul>

            <button type="submit" class="btn btn-primary mb-3">S'enregistrer pour ce jeu </button>
        </div>

        <div class="container bg-light border border-success">
            <h1> Donnez votre avis sur ce jeu   </h1>
            <form>
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Email address</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                  <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">Nom </label>
                  <input type="password" class="form-control" id="exampleInputPassword1">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Commentaire </label>
                    <input type="text" class="form-control" id="exampleInputPassword1">
                </div>
                <button type="submit" class="btn btn-primary">Envoyer</button>
              </form>
        </div>


        <!-- footer -->
        <div class="footer-dark">
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-3 item">
                            <h3>Services</h3>
                            <ul>
                                <li><a href="#">Design web</a></li>
                                <li><a href="#">Developement</a></li>
                                <li><a href="#">Hébergement</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3 item">
                            <h3>A propos </h3>
                            <ul>
                                <li><a href="#">Companie</a></li>
                                <li><a href="#">Equipe</a></li>
                                <li><a href="#">Carriere</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6 item text">
                            <h3>Y_Play</h3>
                            <p>Praesent sed lobortis mi. Suspendisse vel placerat ligula. Vivamus ac sem lacus. Ut vehicula rhoncus elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit pulvinar dictum vel in justo.</p>
                        </div>
                        <div class="col item social"><a href="#"><i class="fa-brands fa-twitter"></i></a><a href="#"><i class="fa-brands fa-linkedin-in"></i></a><a href="#"><i class="fa-brands fa-instagram"></i></a></div>
                    </div>
                    <p class="copyright"> <i class="fa-brands fa-yoast fa-3x text-success"></i>Play© 2023</p>
                </div>
            </footer>
        </div>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>

</html> 