<?php

    include 'controller/connect.php';
    $jeux = $conn->prepare("SELECT * FROM `jeux`"); 
    $jeux->execute();
?>


<!DOCTYPE html5>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
          acceuil
        </title>

        <!-- Liens CDN de Bootstrap -->
        <link rel="stylesheet" type="text/css" href="style/style.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-xCs+HLwMAnqguRZQTO4tMyHiMXJcFASWJu+1XCx0Vp5q+BlBaa+ZuVmFcqZOw83yHxG83u0eLuRU2LNN4mXFDQ==" crossorigin="anonymous" /> -->
    </head>

    <body>

        <!-- barre de navigation -->
        <?php if($user_id == '') include 'view/header.php'; 
                else include 'view/header_user.php';?>
        
        <!-- Petite présentation avec animation de la plateforme -->
        <?php if($user_id == ''){?>
        
        <?php } ?>


        <!-- presentation des jeux  -->
        <h1 class="page-header"> Nos jeux  </h1>
        <div class="card text-center">
            <div class="card-body">
                <div class="row row-cols-1 p-2 row-cols-md-3 g-4" style="align-items: center;">
                <?php if($jeux->rowCount() > 0){
                  $i=1;
                while($jeu = $jeux->fetch(PDO::FETCH_ASSOC)){?>
                    <div class="col">
                      <div class="card p-2 h-100 bg-dark text-light" style="border-radius: 5%;">
                        <center><img src="img/<?= $jeu['photo']; ?>" class="card-img-top" style="height: 300px; width: 300px;" alt="<?= $jeu['nom']; ?>"></center>
                        <div class="card-body" style="width: 100%;">
                          <h5 class="card-title bg-secondary"><?= $jeu['nom']; ?></h5>
                          <p class="card-text"><?= $jeu['description']; ?></p>
                          <?php if($user_id != ''){?>
                          <a href="detail_jeu.php?id_jeu=<?= $jeu['id']; ?>" class="btn btn-success">Voir plus </a>
                          <?php }?>
                        </div>
                      </div>
                    </div>
                    <?php }}?>
                  </div>
              </div>
              
        </div>

        <!-- footer -->
        <?php include 'view/footer.php'; ?>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>

</html> 